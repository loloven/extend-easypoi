package com.loloven.easypoi.extend.export.util;

import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import com.loloven.easypoi.extend.export.service.CustomerExcelExportService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
 * @author wanghaiyang
 * @version 1.0.0
 * @date 2018-12-10 17:37
 **/
public class CustomerExcelExportUtil {

    /**
     * 十万
     */
    private static final int INT_HUNDRED_THOUSAND = 100000;

    private CustomerExcelExportUtil() {
    }

    /**
     * 根据Map创建对应的Excel
     *
     * @param entity     表格标题属性
     * @param entityList Map对象列表
     * @param dataSet    Excel对象数据List
     */
    public static Workbook exportExcel(ExportParams entity, List<ExcelExportEntity> entityList,
                                       Collection<?> dataSet, Function<String, Short> getColor) {
        Workbook workbook = getWorkbook(entity.getType(), dataSet.size());
        new CustomerExcelExportService(getColor)
                .createSheetForMap(workbook, entity, entityList, dataSet);
        return workbook;
    }

    public static Workbook exportExcel(ExportParams entity, List<ExcelExportEntity> entityList,
                                       Collection<?> dataSet) {
        Workbook workbook = getWorkbook(entity.getType(), dataSet.size());
        new CustomerExcelExportService()
                .createSheetForMap(workbook, entity, entityList, dataSet);
        return workbook;
    }

    public static Workbook exportExcel(ExportParams entity, Class<?> pojoClass,
                                       Collection<?> dataSet) {
        Workbook workbook = getWorkbook(entity.getType(), dataSet.size());
        new CustomerExcelExportService().createSheet(workbook, entity, pojoClass, dataSet);
        return workbook;
    }

    private static Workbook getWorkbook(ExcelType type, int size) {
        if (ExcelType.HSSF.equals(type)) {
            return new HSSFWorkbook();
        } else if (size < INT_HUNDRED_THOUSAND) {
            return new XSSFWorkbook();
        } else {
            return new SXSSFWorkbook();
        }
    }
}
