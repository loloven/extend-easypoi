package com.loloven.easypoi.extend.export.service;

import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import cn.afterturn.easypoi.excel.export.ExcelExportService;
import com.loloven.easypoi.extend.export.util.CustomerPoiMergeCellUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @author wanghaiyang
 * @version 1.0.0
 * @date 2018-12-10 17:45
 **/
public class CustomerExcelExportService extends ExcelExportService {

    private Function<String, Short> getColor;

    private static final int INT_TWO = 2;

    public CustomerExcelExportService() {
    }

    public CustomerExcelExportService(Function<String, Short> getColor) {
        super();
        this.getColor = getColor;
    }

    /**
     * 合并单元格
     */
    @Override
    public void mergeCells(Sheet sheet, List<ExcelExportEntity> excelParams, int titleHeight) {
        Map<Integer, int[]> mergeMap = getMergeDataMap(excelParams);
        CustomerPoiMergeCellUtil.mergeCells(sheet, mergeMap, titleHeight);
    }

    private Map<Integer, int[]> getMergeDataMap(List<ExcelExportEntity> excelParams) {
        Map<Integer, int[]> mergeMap = new HashMap<>(0);
        // 设置参数顺序,为之后合并单元格做准备
        int i = 0;
        for (ExcelExportEntity entity : excelParams) {
            if (entity.isMergeVertical()) {
                mergeMap.put(i, entity.getMergeRely());
            }
            if (entity.getList() != null) {
                for (ExcelExportEntity inner : entity.getList()) {
                    if (inner.isMergeVertical()) {
                        mergeMap.put(i, inner.getMergeRely());
                    }
                    i++;
                }
            } else {
                i++;
            }
        }
        return mergeMap;
    }

    @Override
    protected int createHeaderAndTitle(ExportParams entity, Sheet sheet, Workbook workbook,
                                       List<ExcelExportEntity> excelParams) {
        int rows = 0, fieldLength = getFieldLength(excelParams);
        if (entity.getTitle() != null) {
            rows += createTitle2Row(entity, sheet, workbook, fieldLength);
        }
        rows += createHeaderRow(entity, sheet, rows, excelParams);
        sheet.createFreezePane(0, rows, 0, rows);
        return rows;
    }

    /**
     * 创建表头
     */
    private int createHeaderRow(ExportParams title, Sheet sheet, int index,
                                List<ExcelExportEntity> excelParams) {
        Row row = sheet.createRow(index);
        row.setHeight(title.getHeaderHeight());

        Row listRow = null;
        int headerRows = getRowNums(excelParams);
        if (headerRows == INT_TWO) {
            listRow = sheet.createRow(index + 1);
            listRow.setHeight(title.getHeaderHeight());
        }

        int cellIndex = 0;
        int groupCellLength = 0;

        for (int i = 0, exportFieldTitleSize = excelParams.size(); i < exportFieldTitleSize; i++) {
            ExcelExportEntity entity = excelParams.get(i);
            // 设置 titleStyle
            CellStyle titleStyle = getExcelExportStyler().getTitleStyle(title.getColor());
            if (null != getColor) {
                short color = getColor.apply(entity.getKey().toString());
                System.out.println(color);
                titleStyle.setFillForegroundColor(color);
                titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            }
            // 加入换了groupName或者结束就，就把之前的那个换行
            if (StringUtils.isBlank(entity.getGroupName()) || !entity.getGroupName().equals(excelParams.get(i - 1).getGroupName())) {
                if (groupCellLength > 1) {
                    sheet.addMergedRegion(new CellRangeAddress(index, index, cellIndex - groupCellLength, cellIndex - 1));
                }
                groupCellLength = 0;
            }
            if (StringUtils.isNotBlank(entity.getGroupName())) {
                createStringCell(row, cellIndex, entity.getGroupName(), titleStyle, entity);
                if (null != listRow) {
                    createStringCell(listRow, cellIndex, entity.getName(), titleStyle, entity);
                }
                groupCellLength++;
            } else if (StringUtils.isNotBlank(entity.getName())) {
                createStringCell(row, cellIndex, entity.getName(), titleStyle, entity);
            }
            if (entity.getList() != null) {
                List<ExcelExportEntity> sTitel = entity.getList();
                if (StringUtils.isNotBlank(entity.getName()) && sTitel.size() > 1) {
                    sheet.addMergedRegion(new CellRangeAddress(index, index, cellIndex, cellIndex + sTitel.size() - 1));
                }
                for (ExcelExportEntity aSTitel : sTitel) {
                    createStringCell(headerRows == 2 ? listRow : row, cellIndex, aSTitel.getName(),
                            titleStyle, entity);
                    cellIndex++;
                }
                cellIndex--;
            } else if (headerRows == 2 && StringUtils.isBlank(entity.getGroupName())) {
                createStringCell(listRow, cellIndex, "", titleStyle, entity);
                sheet.addMergedRegion(new CellRangeAddress(index, index + 1, cellIndex, cellIndex));
            }
            cellIndex++;
        }
        if (groupCellLength > 1) {
            sheet.addMergedRegion(new CellRangeAddress(index, index, cellIndex - groupCellLength, cellIndex - 1));
        }
        return headerRows;
    }


}
