package com.loloven.easypoi.extend.export.util;

import cn.afterturn.easypoi.excel.entity.params.MergeEntity;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author wanghaiyang
 * @version 1.0.0
 * @date 2019-03-19 16:58
 **/
public class CustomerPoiMergeCellUtil {

    private static Logger log = LoggerFactory.getLogger(CustomerPoiMergeCellUtil.class);

    private CustomerPoiMergeCellUtil() {
    }

    /**
     * 纵向合并相同内容的单元格
     *
     * @param sheet    Sheet
     * @param mergeMap key--列,value--依赖的列,没有传空
     * @param startRow 开始行
     */
    public static void mergeCells(Sheet sheet, Map<Integer, int[]> mergeMap, int startRow) {
        mergeCells(sheet, mergeMap, startRow, sheet.getLastRowNum());
    }

    /**
     * 纵向合并相同内容的单元格
     *
     * @param sheet    Sheet
     * @param mergeMap key--列,value--依赖的列,没有传空
     * @param startRow 开始行
     * @param endRow   结束行
     */
    private static void mergeCells(Sheet sheet, Map<Integer, int[]> mergeMap, int startRow,
                                   int endRow) {
        Map<Integer, List<MergeEntity>> mergeDataMap = new HashMap<>(0);
        if (mergeMap.size() == 0) {
            return;
        }
        Row row;
        Set<Integer> sets = mergeMap.keySet();
        String text;
        for (int i = startRow; i <= endRow; i++) {
            row = sheet.getRow(i);
            for (Integer index : sets) {
                if (row == null || row.getCell(index) == null) {
                    List<MergeEntity> mergeEntities = mergeDataMap.get(index);
                    if (CollectionUtils.isEmpty(mergeEntities)) {
                        continue;
                    }
                    for (MergeEntity mergeEntity : mergeEntities) {
                        if (mergeEntity.getEndRow() == 0) {
                            mergeEntity.setEndRow(i - 1);
                        }
                    }
                } else {
                    Cell cell = row.getCell(index);
                    if (cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
                        text = String.valueOf(cell.getNumericCellValue());
                    } else {
                        text = cell.getStringCellValue();
                    }
                    if (StringUtils.isNotEmpty(text)) {
                        hanlderMergeCells(index, i, text, mergeDataMap, row.getCell(index),
                                mergeMap.get(index));
                    } else {
                        mergeCellOrContinue(index, mergeDataMap);
                    }
                }
            }
        }
        if (mergeDataMap.size() > 0) {
            for (Integer index : mergeDataMap.keySet()) {
                List<MergeEntity> mergeEntitys = mergeDataMap.get(index);
                mergeEntitys.forEach(mergeEntity -> {
                    if (mergeEntity.getEndRow() > mergeEntity.getStartRow()) {
                        checkAndAddMergedRegion(index, sheet, mergeEntity);
                    }

                    for (int rowNo = mergeEntity.getStartRow() + 1; rowNo <= mergeEntity.getEndRow(); rowNo++) {
                        Object cellValue;
                        Cell cell = sheet.getRow(rowNo).getCell(index);
                        if (cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
                            cellValue = cell.getNumericCellValue();
                            sheet.getRow(rowNo).getCell(index).setCellValue(0D);
                        } else {
                            cellValue = cell.getStringCellValue();
                            sheet.getRow(rowNo).getCell(index).setCellValue("");
                        }
                        System.out.println(JSON.toJSONString(cellValue));
                    }
                });
            }
        }

    }

    /**
     * 处理合并单元格
     *
     * @param index        合并列的在合并列集合中的序号
     * @param rowNum       行数
     * @param text         列文本
     * @param mergeDataMap 合并列集合
     * @param cell         Cell
     * @param delys        合并依赖列序号
     */
    private static void hanlderMergeCells(Integer index, int rowNum, String text,
                                          Map<Integer, List<MergeEntity>> mergeDataMap,
                                          Cell cell, int[] delys) {
        try {
            List<MergeEntity> mergeEntitys = mergeDataMap.get(index);
            if (CollectionUtils.isNotEmpty(mergeEntitys)) {
                MergeEntity tempMergeEntity = mergeEntitys.stream()
                        .filter(mergeEntity -> checkIsEqualByCellContents(mergeEntity, text, cell, delys, rowNum))
                        .findFirst().orElse(null);

                if (null != tempMergeEntity) {
                    tempMergeEntity.setEndRow(rowNum);
                } else {
                    mergeEntitys.add(createMergeEntity(text, rowNum, cell, delys));
                }
            } else {
                mergeDataMap.put(index, Lists.newArrayList(createMergeEntity(text, rowNum, cell, delys)));
            }
        } catch (Exception e) {
            log.error("处理合并单元格异常", e);
        }
    }

    private static void checkAndAddMergedRegion(Integer index, Sheet sheet, MergeEntity mergeEntity) {
        CellRangeAddress cellRange = new CellRangeAddress(mergeEntity.getStartRow(),
                mergeEntity.getEndRow(), index, index);
        List<CellRangeAddress> cellRanges = sheet.getMergedRegions();
        if (CollectionUtils.isEmpty(cellRanges) || !cellRanges.contains(cellRange)) {
            sheet.addMergedRegion(cellRange);
        }
    }

    /**
     * 字符为空的情况下判断
     *
     * @param index        合并列的在合并列集合中的序号
     * @param mergeDataMap 合并列集合
     */
    private static void mergeCellOrContinue(Integer index, Map<Integer, List<MergeEntity>> mergeDataMap) {
        List<MergeEntity> mergeEntitys = mergeDataMap.get(index);
        if (CollectionUtils.isNotEmpty(mergeEntitys)) {
            mergeEntitys.forEach(mergeEntity -> {
                if (mergeEntity.getEndRow() != mergeEntity.getStartRow()) {
                    mergeDataMap.remove(index);
                }
            });
        }
    }

    private static MergeEntity createMergeEntity(String text, int rowNum, Cell cell, int[] delys) {
        MergeEntity mergeEntity = new MergeEntity(text, rowNum, rowNum);
        // 存在依赖关系
        if (delys != null && delys.length != 0) {
            List<String> list = new ArrayList<>(delys.length);
            mergeEntity.setRelyList(list);
            for (int dely : delys) {
                list.add(getCellNotNullText(cell, dely, rowNum));
            }
        }
        return mergeEntity;
    }

    private static boolean checkIsEqualByCellContents(MergeEntity mergeEntity, String text,
                                                      Cell cell, int[] delys, int rowNum) {
        // 没有依赖关系
        if (delys == null || delys.length == 0) {
            return mergeEntity.getText().equals(text);
        }
        // 存在依赖关系 测试
        if (mergeEntity.getText().equals(text)) {
            for (int i = 0; i < delys.length; i++) {
                if (mergeEntity.getRelyList().get(i) == null || !mergeEntity.getRelyList().get(i)
                        .equals(getCellNotNullText(cell, delys[i], rowNum))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * 获取一个单元格的值,确保这个单元格必须有值,不然向上查询
     *
     * @param cell   Cell
     * @param index  列序号
     * @param rowNum 行数
     * @return 单元格的值
     */
    private static String getCellNotNullText(Cell cell, int index, int rowNum) {
        if (cell == null || cell.getRow() == null) {
            return null;
        }
        if (cell.getRow().getCell(index) != null
                && StringUtils.isNotEmpty(cell.getRow().getCell(index).getStringCellValue())) {
            return cell.getRow().getCell(index).getStringCellValue();
        }
        return getCellNotNullText(cell.getRow().getSheet().getRow(--rowNum).getCell(index), index,
                rowNum);
    }

}
