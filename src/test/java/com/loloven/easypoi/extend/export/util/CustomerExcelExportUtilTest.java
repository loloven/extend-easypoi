package com.loloven.easypoi.extend.export.util;

import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import com.google.common.collect.Lists;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CustomerExcelExportUtilTest {

    private static Logger log = LoggerFactory.getLogger(CustomerExcelExportUtilTest.class);

    @Test
    public void testSetHeaderColorExportExcel() throws IOException {
        List<ExcelExportEntity> entitys = buildExcelExportEntities();

        ExportParams exportParams = new ExportParams();
        exportParams.setCreateHeadRows(true);
        exportParams.setHeaderColor(HSSFColor.GOLD.index);
        exportParams.setColor(HSSFColor.GOLD.index);

        Workbook workbook = CustomerExcelExportUtil.exportExcel(
                exportParams, entitys, Lists.newArrayList(), getColorByKeyFunction());

        exportFile("表头颜色设置测试", workbook);
    }

    private Function<String, Short> getColorByKeyFunction() {
        return s -> Integer.valueOf(s.split("_")[1]) > 2 ? HSSFColor.YELLOW.index : HSSFColor.LIME.index;
    }

    private List<ExcelExportEntity> buildExcelExportEntities() {
        List<ExcelExportEntity> entitys = new ArrayList<>();

        ExcelExportEntity exportEntity1 = new ExcelExportEntity("列1", "cell_1");
        entitys.add(exportEntity1);
        ExcelExportEntity exportEntity2 = new ExcelExportEntity("列2", "cell_2");
        entitys.add(exportEntity2);
        ExcelExportEntity exportEntity3 = new ExcelExportEntity("列3", "cell_3");
        entitys.add(exportEntity3);

        return entitys;
    }

    @Test
    public void exportExcel1() {
    }

    @Test
    public void exportExcel2() {
    }

    private static void exportFile(String fileName, Workbook workbook) throws IOException {
        File file = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + fileName + ".xlsx");
        System.out.println(file.getCanonicalPath());
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            workbook.write(outputStream);
            outputStream.flush();
            workbook.close();
        } catch (IOException e) {
            log.error("{}|导出|导出文件IO异常", fileName, e);
        }
    }
}
